package com.example.appmenubuttom92

import MiAdaptador
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appmenubuttom92.Database.Alumno
import com.example.appmenubuttom92.Database.dbAlumnos
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import android.widget.SearchView
class AcercaFragment : Fragment() {

    private lateinit var rcvLista: RecyclerView
    private lateinit var adaptador: MiAdaptador
    private lateinit var db: dbAlumnos

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_acerca, container, false)

        rcvLista = view.findViewById(R.id.recId)
        rcvLista.layoutManager = LinearLayoutManager(requireContext())

        db = dbAlumnos(requireContext())
        db.openDataBase()

        val listaAlumnos: ArrayList<Alumno> = db.leerTodos()
        adaptador = MiAdaptador(listaAlumnos, requireContext())
        rcvLista.adapter = adaptador

        db.close()

        view.findViewById<FloatingActionButton>(R.id.agregarAlumno).setOnClickListener {
            val fragment = DbFragment()
            val activity = context as AppCompatActivity
            val currentFragment = activity.supportFragmentManager.findFragmentById(R.id.ac)
            activity.supportFragmentManager.beginTransaction().apply {
                currentFragment?.let {
                    if (it.isVisible) {
                        hide(it)
                    }
                }
                replace(R.id.ac, fragment)
                addToBackStack(null)
                commit()
            }
            (requireActivity() as AppCompatActivity).findViewById<BottomNavigationView>(R.id.btnNavigator)
                .menu.findItem(R.id.btndb).isChecked = true
            view.findViewById<com.google.android.material.floatingactionbutton.FloatingActionButton>(R.id.agregarAlumno)
                .hide()
        }

        val searchView = view.findViewById<SearchView>(R.id.searchView2)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adaptador.filter(newText ?: "")
                return false
            }
        })

        return view
    }
}